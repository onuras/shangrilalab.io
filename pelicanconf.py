AUTHOR = 'Onur Aslan'
SITENAME = 'Onur Aslan'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'UTC'

DEFAULT_LANG = 'en'

DEFAULT_DATE_FORMAT = '%Y-%m-%d'

JINJA_ENVIRONMENT = {
    'trim_blocks': True,
    'lstrip_blocks': True
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = 'themes/onur'
THEME_SETTINGS = {
    'show_categories_in_navigation': False,
}

PLUGINS = [
    'pelican.plugins.webassets',
    'pelican.plugins.render_math',
]

# Blogroll
LINKS = (
    ('Pelican', 'https://getpelican.com/'),
    ('Python.org', 'https://www.python.org/'),
    ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
    ('You can modify those links in your config file', '#'),
)

# Social widget
SOCIAL = (
    ('You can add links in your config file', '#'),
    ('Another social link', '#'),
)

DEFAULT_PAGINATION = False

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {
            'css_class': 'highlight',
        },
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {
            'title': 'Table of Contents',
        },
    },
    'output_format': 'html5',
}

STATIC_PATHS = [
    'static',
    'static/robots.txt',
    'static/favicon.ico',
    'static/favicon.svg',
    'cv/cv.pdf',
]

EXTRA_PATH_METADATA = {
    'static/robots.txt': {
        'path': 'robots.txt'
    },
    'static/favicon.svg': {
        'path': 'favicon.svg'
    },
    'static/favicon.ico': {
        'path': 'favicon.ico'
    },
    'cv/cv.pdf': {
        'path': 'cv.pdf'
    },
}

# URL settings
ARCHIVES_URL = 'archives/'
ARCHIVES_SAVE_AS = 'archives/index.html'
ARTICLE_URL = '{category}/{slug}/'
ARTICLE_SAVE_AS = '{category}/{slug}/index.html'
ARTICLE_LANG_URL = '{category}/{slug}/'
ARTICLE_LANG_SAVE_AS = '{category}/{slug}/index.html'
AUTHOR_URL = 'author/{slug}/'
AUTHOR_SAVE_AS = 'author/{slug}/index.html'
AUTHORS_URL = 'authors/'
AUTHORS_SAVE_AS = 'authors/index.html'
CATEGORY_URL = '{slug}/'
CATEGORY_SAVE_AS = '{slug}/index.html'
CATEGORIES_URL = 'categories/'
CATEGORIES_SAVE_AS = 'categories/index.html'
DRAFT_URL = 'drafts/{category}/{slug}/'
DRAFT_SAVE_AS = 'drafts/{category}/{slug}/index.html'
DRAFT_LANG_URL = 'drafts/{category}/{slug}/'
DRAFT_LANG_SAVE_AS = 'drafts/{category}/{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
PAGE_LANG_URL = '{slug}/{lang}/'
PAGE_LANG_SAVE_AS = '{slug}/{lang}/index.html'
TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = 'tag/{slug}/index.html'
TAGS_URL = 'tags/'
TAGS_SAVE_AS = 'tags/index.html'
