Title: LXC altında Steam Kurulumu
Date: Tue, 18 Jan 2022 15:00:12 +0300
Lang: tr

Eğer siz de benim gibi sisteminizde non-free yazılım çalıştırmaktan
hoşlanmıyor, yine de oyun oynamayı seviyorsanız, hiç bir performans kaybı
olmadan LXC ile tamamen izole edilmiş bir container'a Steam kurup oyunlarınızı
oynayabilirsiniz.  Üstelik bunu da tamamıyle *unprivileged*, yani root
yetkisine sahip olmadan yapabilirsiniz.

Bu yazı Debian 11 (bullseye) için yazılmıştır.

Öncelikle sisteminize `lxc` paketini apt ile kurun:

```sh
sudo apt install lxc
```

Ardından yerel kullanıcıya lxc network'unu kullanabilmesi için yetki verin:

```sh
echo onur veth lxcbr0 100 | sudo tee /etc/lxc/lxc-usernet
```

Root ile hostta çalıştırmanız gereken komutlar sadece bu kadar. Artık geri kalan
tüm işlemleri hiç bir root yetkisine ihtiyaç duymadan normal kullanıcınız ile
yapabilirsiniz.

Ardından `~/.config/lxc/default.conf` dosyasını oluşturup aşağıdakileri ekleyin:

```
lxc.include = /etc/lxc/default.conf

lxc.idmap = u 0 100000 1000
lxc.idmap = g 0 100000 1000
lxc.idmap = u 1000 1000 1
lxc.idmap = g 1000 1000 1
lxc.idmap = u 1001 101000 64535
lxc.idmap = g 1001 101000 64535

lxc.apparmor.profile = unconfined
```

Oluşturmuş olduğunuz `idmap`, containerlar içerisinde 0 uid'sini hostta 100000
idsine çevirmeye yarıyor. Aynı zamanda hostta bulunan 1000 idli kullanıcıyı
da ayni şekilde container içerisinde 1000 idsi ile kullanıyoruz.

### Container Oluşturma

Artık LXC sistemde hazır olduğuna göre container oluşturabiliriniz. Bunun için
`lxc-create` komutunu kullanacağız:

```sh
lxc-create -n steam -t download -- -d debian -r bullseye -a amd64 --keyserver hkp://keyserver.ubuntu.com
```

Bu komut `steam` isimli debian bullseye container'ı oluşturacaktır. Ekran kartı
ve pulseaudio'u container içerisinde kullanabilmemiz için bazı mountlar yapmamız
gerekli. `~/.local/share/lxc/steam/config` dosyasını açın ve aşağıda yer alan
mount girdilerini ekleyin, ben nvidia ekran kartı kullandığım için gerekli
nvidia aygıtlarını da ekledim:

```
lxc.mount.entry = /tmp/.X11-unix tmp/.X11-unix none bind,create=dir
lxc.mount.entry = /dev/dri dev/dri none bind,create=dir
lxc.mount.entry = /run/user/1000/pulse/native tmp/.pulse-native none bind,create=file
lxc.mount.entry = /dev/nvidia0 dev/nvidia0 none bind,optional,create=file
lxc.mount.entry = /dev/nvidiactl dev/nvidiactl none bind,optional,create=file
lxc.mount.entry = /dev/nvidia-modeset dev/nvidia-modeset none bind,optional,create=file
```

Artık container'ınızı başlatabilirsiniz:

```
lxc-unpriv-start -n steam
```

Container başladıktan sonra attach olup kendi kullanıcınızı oluşturabilir ve steam
kurabilirsiniz. Öncelikle container'a attach olalım:

```
lxc-unpriv-attach -n steam --clear-env
```

Container içerisinde çalıştırmanız gereken komutlar:

```sh
# Yeni kullanıcı oluşturulması
adduser onur --disabled-login --disabled-password --gecos ''

# Debian contrib ve non-free depolarının eklenmesi
echo deb http://deb.debian.org/debian bullseye main contrib non-free | tee /etc/apt/sources.list

# Steam için gerekli i386 mimarisinin eklenmesi
dpkg --add-architecture i386

# Depoların update edilmesi
apt update

# nvidia sürücülerinin kurulması
# nvidia ekran kartı kullanmıyorsanız bu adımı atlayın
apt install --no-install-recommends nvidia-driver-libs:amd64 nvidia-driver-libs:i386

# Steam kurulması
apt install steam
```

Artık container'ınız hazır. İsteğinize göre playonlinux veya wine da kurabilirsiniz.
Kurulumları tamamladıktan sonra root olduğunuz containerdan çıkın.

Son bir adım olarak host makinede çalışan xorg'a container'ın bağlanabilmesi için
yetki vermemiz gerekli. Bunun bir çok yolu var, ben `xhost +` komutu ile access
control'u tamamen disable ediyorum.

Artık steam'i başlatabilirsiniz:

```
lxc-unpriv-attach -n steam -u 1000 -g 1000 -v PULSE_SERVER=unix:/tmp/.pulse-native steam
```
