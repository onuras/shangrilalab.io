Title: Home Entertainment System
Date: 2024-09-06T15:44:53+03:00
Status: draft

<!--
I finally built a system that I can access anywhere, get best picture output,
and runs fast with extremely low maintenance cost. Idea is turning everything
into self-hosted services and get rid of all cloud subscriptions. I am tired of
seeing my favorite TV shows getting removed from *X* video streaming platform
while I am still watching them.
-->


## Hardware

#### NAS

1. <a href="https://ae01.alicdn.com/kf/S20ccf8787fea4247bdc69adc897a2ba82.jpg" target="_blank"><img src="https://ae01.alicdn.com/kf/S20ccf8787fea4247bdc69adc897a2ba82.jpg" alt="Topton J6413 NAS motherboard specs" class="is-right" /></a>
    **Topton J6413 NAS motherboard**: Cute low power Mini ITX motherboard with
    embedded Intel Celeron J6413 processor. This is only Topton motherboard I
    could buy in my country. Go with N100 if you are gonna build your system
    today, they come with 10Gb ethernet. CPU that comes with this motherboard
    is enough for my needs.  Some of the features of motherboard:

    1. Intel Celeron J6413 processor (4 Core, 3.0 GHz max boost speed).
    2. 2x PCIe 3.0 M.2 NVMe slots.
    3. 6x SATA 3.0 port with JMB585 controller.
    4. 4x 10Gb Ethernet.

    <a href="https://ae01.alicdn.com/kf/S20ccf8787fea4247bdc69adc897a2ba82.jpg" target="_blank">Click here to see full list of specs.</a>

2. **2x 32 GB Kingson 3200MHz SO-DIMM DDR4 memory**: This is maximum amount of
    memory motherboard supports.

3. **2x 1TB Kioxia Exceria Plus G3 NVMe SSD disks**: This is a bit overkill
    since J6413 only supports PCIe 3.0, but I got them for a really good price
    (cheaper than PCIe 3.0 disks) and I am a big fan of Kioxia. They will be
    used as boot/os disks in a RAID1 configuration.

4. **Fractal Node 304 Case**: I can't tell how much I love this case. It is one
    of the best home NAS case if you have space constraints. It supports Mini
    DTX motherboards and comes with 6x 3.5" inch HDD slots. Size of this case
    is super compact, you can fit it behind TV. It also have very active modding
    community.

5. **Thermaltake PSU**: This is some random PSU I've been using for years.


#### Gaming Machine

I am an avid gamer, and I prefer to play games in-front of TV. Here is the 

1. **JGINYUE B760i motherboard**: Mini ITX motherboards are expensive, this one is cheap. I just wanted to test it. It just works.
2. **Intel Core i5-13600KF**
3. **DeepCool AN600**
4. **Kingston KVR32N22S8/16 16GB (2x16GB)**
5. **Kioxia Exceria Pro 1 TB**
6. **Kioxia Exceria Pro 1 TB**
7. **Lzmod A24-V5 Case**
8. **Silverstone SST-FX500-G**
9. **GIGABYTE RTX 4060 OC LP**
10. **Intel AX201NGW**

