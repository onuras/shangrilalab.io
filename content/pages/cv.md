Title: CV

<div class="row">
  <div class="col-6-md">
    Onur Aslan<br/>
    DevOps Engineer - Certified Kubernetes Administrator
  </div>
  <div class="col-6-md text-right">
    Email: <a href="mailto:onur@onur.im">onur@onur.im</a><br/>
    Mobile: +90 546 865 39 55
  </div>
</div>


## Professional Experience

#### **[Atölye15](https://www.atolye15.com/) (DevOps Engineer) / Remote / October 2023 &mdash; Present**

* **Platform Engineer**: Planned, designed and deployed entire Cloud
    Infrastructure on Azure for multiple projects with Terraform.
* **Kubernetes**: Setup and managed multiple Kubernetes clusters for
    different environments, entirely Infrastructure as Code with ArgoCD
    bootstraping by following GitOps practices.
* **Helm**: Developed and maintained helm charts for various projects.
* **Monitoring**: Setup and maintained monitoring with kube-prometheus-stack
    (Prometheus/Grafana/Loki). Created alerts and notifications for important
    events.
* **Observability**: Setup and maintained observability platform with
    OpenTelemetry/Tempo/Grafana in a most cost efficient way.
* **CI/CD**: Developed and maintained CI/CD pipelines on Azure DevOps pipelines
    and GitHub actions for hundreds of microservices.
* **Elasticsearch**: Setup and managed Elasticsearch cluster in Kubernetes with
    Elasticsearch operator.


#### **[Namecheap](https://www.namecheap.com/) (Site Reliability Engineer) / Remote / June 2022 &mdash; October 2023**

* **Monitoring**: Monitored and optimized Kubernetes clusters to ensure high
    availability and performance. 
* **Alerts**: Implemented robust monitoring and alerting systems to proactively
    identify and resolve issues.
* **Scripting**: Created and maintained series of bash scripts for customer
    migrations to new infrastructure.
* **Networking**: Maintained network/BGP announcements with Netbox.
* **Kubernetes**: Maintained kubernetes clusters created with Rancher and OpenStack.
* **Helm**: Developed and maintained helm charts for various projects.
    Used helmfile plugin extensively.


#### **[DefensX](https://www.defensx.com/) (DevOps Engineer) / Remote / Feb 2019 &mdash; June 2022**

* **Infrastructure Architect**: Designed, installed, managed and maintained company’s entire infrastructure on
    Azure with Terraform and Ansible. Been responsible from authorization, security, maintenance, PKI and VPN.
    Followed best practices like DRY and modularization for Terraform.
* **Kubernetes Deployments**: Developed kubectl plugin to manage deployments to many Kubernetes clusters.
    Been responsible from scalable, mission-critical Kubernetes deployments to AKS, EKS and DO.
* **Kubernetes Servers**: Installed and maintained Kubernetes clusters on Azure, AWS, DO and bare-metal servers.
* **Automation**: Dockerized, developed CI/CD pipelines and automated deployments of every microservices.
* **Security Scans**: Implemented periodical security scans and security updates/fixes to every image.
* **Monitoring**: Setup monitoring tools; Prometheus, Loki and Grafana. Provided informative dashboards.
* **Rust Development**: Developed bindings and high performance service for URL categorization engine with Rust.
* **Python Development**: Developed OAuth2 based login provider with Python FastAPI and Vue.js, and integrated
    with Azure AD and Okta.


## Volunteer Experience

#### **[Mozilla - The Rust Programming Language](https://www.rust-lang.org/governance/teams/dev-tools#Docs.rs%20team) / Software/DevOps Engineer / Jan 2016 &mdash; Feb 2022**

* **Rust Development**: Developed fully automated and roboust documentation builder and hosting service for the
    Rust Programming Language libraries: Docs.rs sponsored by Leaseweb and later Amazon.
* **System Administration**: Managed bare metal dedicated Debian servers and later AWS EC2 instances.
* **LXC and Docker**: Used LXC and configured Linux cgroups. Secured containers with Linux PAM modules.
    Managed more than thousands of system dependencies to provide high successfully build rate. Later started using
    sandboxed dispossable Docker containers for build environments.
* **PostgreSQL DBA**: Setup and optimized hand crafted PostgreSQL database with the size of 1 TB.
* **CI/CD Automation**: Developed CI/CD pipelines with Azure Pipelines to build Docker images and automatized
    deployments to AWS EC2 instances.
* **Rust Programming Language Member**: Worked in various teams

#### **[Debian Project](https://contributors.debian.org/contributor/onur-guest@alioth/) / Package Maintainer / Aug 2010 - Present**

* **Teams**: Worked in Python and Perl teams to build Debian packages for various open source projects.
* **Patching**: Patched various projects to make it work with system libraries and dependencies.
* **Testing**: Used low level Debian packaging tools to make proper packages and used QA and test tools to test
    packages in Debian and other Debian derivative distributions.
* **Bugs**: Developed patches and solved bugs with Debian BTS (Bug Tracking System).
* **Ports**: Ported packages to other Debian derived distributions (mainly Ubuntu).


## Skills

* **DevOps**: [Kubernetes (CKA)](https://onuraslan.com/OnurAslanCKA.pdf), Helm/Kustomize, Azure, Terraform, Prometheus/Loki/Grafana, GitLab CI/CD, GitHub Actions, Linux
* **SRE**: Kubernetes, Prometheus, Grafana, Alertmanager, Monitoring, Alerting, Incident Response
* **Programming**: Rust, Python, Bash, JavaScript, PostgreSQL


## Education

K. Maraş Sütçü İmam University / AAS in Computer Science; GPA: 3.90 / 2008 &mdash; 2010
